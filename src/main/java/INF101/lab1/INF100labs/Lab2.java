package INF101.lab1.INF100labs;

public class Lab2 {

    public static void main(String[] args) {
        // Oppgave 1: Test findLongestWord-metoden
        findLongestWords("Game", "Action", "Champion");
        findLongestWords("apple", "carrot", "ananas");
        findLongestWords("Four", "Five", "Nine");

        // Oppgave 2: Test isLeapYear-metoden
        boolean leapYear1 = isLeapYear(2022);
        System.out.println(leapYear1); // false

        boolean leapYear2 = isLeapYear(1996);
        System.out.println(leapYear2); // true

        boolean leapYear3 = isLeapYear(1900);
        System.out.println(leapYear3); // false

        boolean leapYear4 = isLeapYear(2000);
        System.out.println(leapYear4); // true

        // Oppgave 3: Test isEvenPositiveInt-metoden
        boolean evenPositive1 = isEvenPositiveInt(123456);
        System.out.println(evenPositive1); // true

        boolean evenPositive2 = isEvenPositiveInt(-2);
        System.out.println(evenPositive2); // false

        boolean evenPositive3 = isEvenPositiveInt(123);
        System.out.println(evenPositive3); // false
    }

////////////////////////////////////////// T A S K 1 //////////////////////////////////////////findLongestWord-metoden
    public static void findLongestWords(String word1, String word2, String word3) {
        int maxLength = Math.max(Math.max(word1.length(), word2.length()), word3.length());

        // Skriv ut alle ord med samme lengde som det lengste
        if (word1.length() == maxLength) {
            System.out.println(word1);
        }
        if (word2.length() == maxLength) {
            System.out.println(word2);
        }
        if (word3.length() == maxLength) {
            System.out.println(word3);
        }
    }

////////////////////////////////////////// T A S K 2 //////////////////////////////////////////isLeapYear-metoden
    public static boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                return year % 400 == 0;
            }
            return true;
        } else {
            return false;
        }
    }

////////////////////////////////////////// T A S K 3 //////////////////////////////////////////isEvenPositiveInt-metoden
    public static boolean isEvenPositiveInt(int num) {
        return num > 0 && num % 2 == 0;
    }
}
