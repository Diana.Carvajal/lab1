package INF101.lab1.INF100labs;
import java.util.Scanner;


public class Lab1 {
    
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        task1(); task2();
            }

//////////////////////////////////////////// T A S K 1 //////////////////////////////////////////////////        

    public static void task1() {
        System.out.println("Hei, det er meg, datamaskinen.");
        System.out.println("Hyggelig å se deg her.");
        System.out.println("Lykke til med INF101!");
        System.out.println();

    }

/////////////////////////////////////////// T A S K 2 //////////////////////////////////////////////////        

    public static void task2() {
        sc = new Scanner(System.in); // Do not remove this line
        
     // Les inn brukerens navn, gateadresse, postnummer og poststed, ved å bruke readInput.
     String navn = readInput("Hva er ditt navn?");
     String adresse = readInput("Hva er din adresse?");
     String postinfo = readInput("Hva er ditt postnummer og poststed?"); 

     // Printer "navn-s adresse er:"
     System.out.println(navn + "s adresse er:");

     System.out.println();

     System.out.println(navn);
     System.out.println(adresse);
     System.out.println(postinfo);
    
 }
    

 public static String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine();
        return userInput;
    }

}
