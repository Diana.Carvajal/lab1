package INF101.lab1.INF100labs;

public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods to test them on different inputs
        multiplesOfSevenUpTo(49);
        System.out.println();
        multiplicationTable(3);
        System.out.println();
        multiplicationTable(5);
        System.out.println();
        int sum1 = crossSum(1);
        System.out.println(sum1);
        int sum2 = crossSum(12);
        System.out.println(sum2);
        int sum3 = crossSum(123);
        System.out.println(sum3);
        int sum4 = crossSum(1234);
        System.out.println(sum4);
        int sum5 = crossSum(4321);
        System.out.println(sum5);
    }
 
    ////////////////////////////////////////// T A S K 1 //////////////////////////////////////////multiplesOfSevenUpTo

    public static void multiplesOfSevenUpTo(int n) {
        int num = 7;
        while (num <= n) {
            System.out.println(num);
            num += 7;
        }
    }

    ////////////////////////////////////////// T A S K 2 //////////////////////////////////////////multiplicationTable

    public static void multiplicationTable(int n) {
        for (int i = 1; i <= n; i++) {
            System.out.print(i + ": ");
            for (int j = 1; j <= n; j++) {
                System.out.print(i * j + " ");
            }
            System.out.println();
        }
    }

    ////////////////////////////////////////// T A S K 3 //////////////////////////////////////////crossSum

    public static int crossSum(int num) {
        int sum = 0;
        while (num > 0) {
            sum += num % 10;
            num /= 10;
        }
        return sum;
    }
}
