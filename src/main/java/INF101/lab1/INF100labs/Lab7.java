package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

        // Task 1 - removeRow
        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid1.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid1.add(new ArrayList<>(Arrays.asList(31, 32, 33)));

        removeRow(grid1, 0);
        for (int i = 0; i < grid1.size(); i++) {
            System.out.println(grid1.get(i));
        }


        ArrayList<ArrayList<Integer>> grid2 = new ArrayList<>();
        grid2.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid2.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid2.add(new ArrayList<>(Arrays.asList(31, 32, 33)));

        removeRow(grid2, 1);
        for (int i = 0; i < grid2.size(); i++) {
            System.out.println(grid2.get(i));
        }


        // Task 2 - allRowsAndColsAreEqualSum
        ArrayList<ArrayList<Integer>> grid3 = new ArrayList<>();
        grid3.add(new ArrayList<>(Arrays.asList(3, 0, 9)));
        grid3.add(new ArrayList<>(Arrays.asList(4, 5, 3)));
        grid3.add(new ArrayList<>(Arrays.asList(6, 8, 1)));

        boolean equalSums1 = allRowsAndColsAreEqualSum(grid3);
        System.out.println(equalSums1); // false

        ArrayList<ArrayList<Integer>> grid4 = new ArrayList<>();
        grid4.add(new ArrayList<>(Arrays.asList(3, 4, 6)));
        grid4.add(new ArrayList<>(Arrays.asList(0, 5, 8)));
        grid4.add(new ArrayList<>(Arrays.asList(9, 3, 1)));

        boolean equalSums2 = allRowsAndColsAreEqualSum(grid4);
        System.out.println(equalSums2); // false

        ArrayList<ArrayList<Integer>> grid5 = new ArrayList<>();
        grid5.add(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));
        grid5.add(new ArrayList<>(Arrays.asList(2, 3, 4, 1)));
        grid5.add(new ArrayList<>(Arrays.asList(3, 4, 1, 2)));
        grid5.add(new ArrayList<>(Arrays.asList(4, 1, 2, 3)));

        boolean equalSums3 = allRowsAndColsAreEqualSum(grid5);
        System.out.println(equalSums3); // true
    }

    ////////////////////////////////////////// Task 1 - removeRow //////////////////////////////////////////

    /**
     * Removes the specified row from the 2D grid.
     *
     * @param grid The 2D grid of integers.
     * @param row  The index of the row to be removed.
     */
    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    ////////////////////////////////////////// Task 2 - allRowsAndColsAreEqualSum //////////////////////////////////////////

    /**
     * Checks if all rows and columns in the 2D grid have equal sums.
     *
     * @param grid The 2D grid of integers.
     * @return True if all rows and columns have equal sums, false otherwise.
     */
    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        // Check if all rows have equal sums
        int rowSum = grid.get(0).stream().mapToInt(Integer::intValue).sum();
        for (ArrayList<Integer> row : grid) {
            if (row.stream().mapToInt(Integer::intValue).sum() != rowSum) {
                return false;
            }
        }

        // Check if all columns have equal sums
        for (int col = 0; col < grid.get(0).size(); col++) {
            int sum = 0;
            for (ArrayList<Integer> row : grid) {
                sum += row.get(col);
            }
            if (sum != rowSum) {
                return false;
            }
        }

        return true;
    }
}
