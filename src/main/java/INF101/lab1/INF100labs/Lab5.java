package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class Lab5 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

        // Task 1 - removeThrees
        ArrayList<Integer> list1 = new ArrayList<>(List.of(1, 2, 3, 4));
        ArrayList<Integer> removedList1 = removeThrees(list1);
        System.out.println(removedList1); // [1, 2, 4]

        ArrayList<Integer> list2 = new ArrayList<>(List.of(1, 2, 3, 3));
        ArrayList<Integer> removedList2 = removeThrees(list2);
        System.out.println(removedList2); // [1, 2]

        ArrayList<Integer> list3 = new ArrayList<>(List.of(3, 3, 1, 3, 2, 4, 3, 3, 3));
        ArrayList<Integer> removedList3 = removeThrees(list3);
        System.out.println(removedList3); // [1, 2, 4]

        ArrayList<Integer> list4 = new ArrayList<>(List.of(3, 3));
        ArrayList<Integer> removedList4 = removeThrees(list4);
        System.out.println(removedList4); // []

        // Task 2 - uniqueValues
        ArrayList<Integer> list5 = new ArrayList<>(List.of(1, 1, 2, 1, 3, 3, 3, 2));
        ArrayList<Integer> uniqueList1 = uniqueValues(list5);
        System.out.println(uniqueList1); // [1, 2, 3]

        ArrayList<Integer> list6 = new ArrayList<>(List.of(4, 4, 4, 4, 4, 4, 4, 4, 4, 5));
        ArrayList<Integer> uniqueList2 = uniqueValues(list6);
        System.out.println(uniqueList2); // [4, 5]

        // Task 3 - addList
        ArrayList<Integer> a1 = new ArrayList<>(List.of(1, 2, 3));
        ArrayList<Integer> b1 = new ArrayList<>(List.of(4, 2, -3));
        addList(a1, b1);
        System.out.println(a1); // [5, 4, 0]

        ArrayList<Integer> a2 = new ArrayList<>(List.of(1, 2, 3));
        ArrayList<Integer> b2 = new ArrayList<>(List.of(47, 21, -30));
        addList(a2, b2);
        System.out.println(a2); // [48, 23, -27]
    }

    ////////////////////////////////////////// Task 1 - removeThrees //////////////////////////////////////////

    /**
     * Removes all occurrences of the number 3 from the given list.
     * @param list The input list of integers.
     * @return A new list with all occurrences of 3 removed.
     */
    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        list.removeIf(i -> i == 3);
        return list;
    }

    ////////////////////////////////////////// Task 2 - uniqueValues //////////////////////////////////////////

    /**
     * Returns a new list containing only unique values from the input list.
     * @param list The input list of integers.
     * @return A new list with duplicate values removed.
     */
    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        Set<Integer> uniqueSet = new HashSet<>(list); // Using a Set to automatically remove duplicates
        return new ArrayList<>(uniqueSet);
    }

    ////////////////////////////////////////// Task 3 - addList //////////////////////////////////////////

    /**
     * Adds corresponding elements of two lists and modifies the first list.
     * @param a The first list of integers.
     * @param b The second list of integers.
     */
    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size() && i < b.size(); i++) {
            a.set(i, a.get(i) + b.get(i));
        }
    }
}
